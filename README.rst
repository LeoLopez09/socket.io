SOCKET.IO
==============

Pasos para ejecutar Node js en local.
------------------------------------------------

Primero
--------

Clonar el proyecto::

	$ git clone https://LeoLopez09@bitbucket.org/LeoLopez09/socket.io.git

Segundo
--------

Correr el servidor::

	$ node index.js

Tercero
--------

Se debe optener la dirección IP, para poder remplazarla en el proyecto de xcode Product Manager https://bitbucket.org/LeoLopez09/product-manager/src/master/ el archivo SocketIOManager.swift linea 14.


Licencia
--------------

Leonardo Flórez López
